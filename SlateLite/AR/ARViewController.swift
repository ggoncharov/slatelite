//
//  ARViewController.swift
//  SlateLite
//
//  Created by Gotlib on 15.04.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

@available(iOS 11.3, *)
class ARViewController: UIViewController, ARSCNViewDelegate {
    
    static let sharedInstance = ARViewController()
    var sessionConfig: ARConfiguration = ARWorldTrackingConfiguration()

    @IBAction func restartButtonAction(_ sender: UIButton) {
        
        if let worldSessionConfig = self.sessionConfig as? ARWorldTrackingConfiguration {
            worldSessionConfig.planeDetection = .vertical
            self.arScreen.session.run(worldSessionConfig, options: [.resetTracking, .removeExistingAnchors])
        }
        self.isPassed = false
    }
    var imageName = String()
    @IBOutlet weak var arScreen: ARSCNView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.arScreen.delegate = self
        
        self.arScreen.showsStatistics = true
        self.arScreen.autoenablesDefaultLighting = true
//        self.arScreen.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isPassed = false
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        // Tell the session to automatically detect horizontal planes
        configuration.planeDetection = .vertical
        
        // Run the view's session
        arScreen.session.run(configuration)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        arScreen.session.pause()
    }
    
    func createPlaneNode(anchor: ARPlaneAnchor) -> SCNNode {
        // Create a SceneKit plane to visualize the node using its position and extent.
        
        // Create the geometry and its materials
//        let plane = SCNPlane(width: CGFloat(anchor.extent.x), height: CGFloat(anchor.extent.z))
        let lavaImage = UIImage(named: ARViewController.sharedInstance.imageName) //UIImage(named: "clearwhitestripe01.png")//ARViewController.sharedInstance.imageName)
        let width = 1.220 //(lavaImage?.size.width)! / 10
        let height =  0.61 //(lavaImage?.size.height)! / 10
        let plane = SCNPlane(width: CGFloat(width), height: CGFloat(height))

        let lavaMaterial = SCNMaterial()
        lavaMaterial.diffuse.contents = lavaImage
        lavaMaterial.isDoubleSided = true
        
        plane.materials = [lavaMaterial]
        
        // Create a node with the plane geometry we created
        let planeNode = SCNNode(geometry: plane)
        planeNode.position = SCNVector3Make(anchor.center.x, 0, anchor.center.z)
        
        // SCNPlanes are vertically oriented in their local coordinate space.
        // Rotate it to match the horizontal orientation of the ARPlaneAnchor.
        planeNode.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
        self.isPassed = true
        return planeNode
    }
    var isPassed = Bool()
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if !self.isPassed {
            guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
           
            let planeNode = createPlaneNode(anchor: planeAnchor)
            
            // ARKit owns the node corresponding to the anchor, so make the plane a child node.
            node.addChildNode(planeNode)
        }
    }
    
    // When a detected plane is updated, make a new planeNode
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
         if !self.isPassed {
            guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
            
            // Remove existing plane nodes
            node.enumerateChildNodes {
                (childNode, _) in
                childNode.removeFromParentNode()
            }
            
            
            let planeNode = createPlaneNode(anchor: planeAnchor)
            
            node.addChildNode(planeNode)
        }
    }
    
    // When a detected plane is removed, remove the planeNode
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        guard anchor is ARPlaneAnchor else { return }
        
        // Remove existing plane nodes
        node.enumerateChildNodes {
            (childNode, _) in
            childNode.removeFromParentNode()
        }
    }
    
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
