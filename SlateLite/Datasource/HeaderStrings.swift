//
//  HeaderStrings.swift
//  SlateLite
//
//  Created by Maxym Glukhov on 04.05.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import Foundation

class HeaderStrings {
    
    static func headerText() -> String {
        
        switch MainPageDataSource.sharedInstance.current_open_list {
        case .slateLite:
            return slateliteHeaderText
        case .ecostone:
            return ecostoneHeaderText
        case .trans:
            return transHeaderText
        }
    }
    
    internal static let slateliteHeaderText = NSLocalizedString("Slate-Lite_header", comment: "")//"Slate-Lite - каменный шпон - это революционный материал, состоящий из тонкого слоя натурального камня на стекловолоконной основе. Производятся следующие стандартные форматы листов: 122 х 61 см, 240 х 120 см, 260 х 120 см. Под заказ могут быть произведены листы формата до 130 х 300 см. Возможности применения Slate-Lite практически безграничны: настенные и напольные покрытия, мебель и душевые кабины, фасады домов и двери."
    internal static let ecostoneHeaderText = NSLocalizedString("Eco-Stone_header", comment: "")// "Slate-Lite Eco - каменный шпон - это революционный материал, состоящий из тонкого слоя камня на хлопковой основе. Этот материал идеально подходит для отделки сложных форм, колонн с малым радиусом и 3D формования. Производятся следующие стандартные форматы листов: 122 х 61 см, 240 х 120 см, 260 х 120 см. Под заказ могут быть произведены листы формата до 130 х 300 см"
    internal static let transHeaderText = NSLocalizedString("Trans_header", comment: "")//"Большинство декоров Slate-Lite могут быть произведены светопроницаемыми. Источник света с задней стороны листа создаст изумительную картину из узоров и света. Мы поможем вам создать конструктив для Вашего индивидуального проекта и реализуем его в кратчайшие сроки. Производятся следующие стандартные форматы листов: 122 х 61 см, 240 х 120 см, 260 х 120 см."
}
