//
//  Tile.swift
//  SlateLite
//
//  Created by Maxym Glukhov on 04.05.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import Foundation

class Tile : CustomStringConvertible
{
    
    /// name of tile
    let name: String
    /// factory name
    let belongTo: [String]
    /// size of tile for each factory that is avalible
    let sizeIn: [String:[String]]
    /// size of tile for each factory to order
    let sizeOut: [String:[String]]
    /// list of image names
    let photos: [String]
    /// name of main image
    let main: String
    /// name of image for ARKit
    let main_1: String
    /// price list
    let price: [String:[String]]
    
    /// initer
    ///
    /// - Parameter jsonDictinary: json object from file
    init(jsonDictinary: [String:Any]) {
        
        self.name = jsonDictinary["name"] as! String
        self.main = jsonDictinary["main"] as! String
        self.main_1 = jsonDictinary["main_1"] as! String
        
        self.belongTo = jsonDictinary["belongTo"] as! [String]
        self.photos = jsonDictinary["photos"] as! [String]
        
        self.sizeIn = jsonDictinary["sizeIn"] as! [String:[String]]
        self.sizeOut = jsonDictinary["sizeOut"] as! [String:[String]]
        self.price = jsonDictinary["price"] as! [String:[String]]
    }
    
    


    
    var description: String {
        return "{\n name = \(self.name),\n belongTo = \(self.belongTo),\n photos = \(self.photos),\n sizeIn = \(self.sizeIn)\n}"
    }
}
