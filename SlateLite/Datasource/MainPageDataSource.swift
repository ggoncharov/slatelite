//
//  MainPageDataSource.swift
//  SlateLite
//
//  Created by Gotlib on 04.05.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import Foundation

enum CurrentOpenList: Int {
    case slateLite = 1
    case ecostone = 2
    case trans = 3
}

class MainPageDataSource{
    
    /// Singletone instace
    static let sharedInstance = MainPageDataSource();
    
    /// current selected type of array
    var current_open_list:CurrentOpenList = .slateLite

    /// array of all tiles
    private let data: [Tile]
    
    /// list of tiles that belong to SlateLite
    var currentData:[Tile]{
        
        get{
            
            return self.data.filter( { (tile: Tile) -> Bool in
                switch self.current_open_list
                {
                    case .slateLite:
                        return tile.belongTo.contains("SlateLite")
                    case .ecostone:
                        return tile.belongTo.contains("ECOSTONE")
                    case .trans:
                        return tile.belongTo.contains("TRANS")
                }
            })
        }
    }
    
    
    
    /// Initer on which we load data array
    init() {
        
        if let path = Bundle.main.path(forResource: "data", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                guard let dataArray = jsonResult as? [[String:Any]] else {
                    
                    self.data = [] 
                    print("SMTH HAPPENS CHECK DATA FILE!!!!")
                    return
                }
                var result = [Tile]()
                
                for jsonDict in dataArray{
                    
                    result.append(Tile(jsonDictinary: jsonDict))
                }
                
                self.data = result
            } catch {
                
                self.data = []
                print("SMTH HAPPENS CHECK DATA FILE!!!!")
            }
        }else{
            
            self.data = []
            print("SMTH HAPPENS  DATA FILE IS MISSING !!!!")
        }
    }
    
    
}
