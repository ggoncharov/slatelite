//
//  Network.swift
//  SlateLite
//
//  Created by Gotlib on 02.02.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit
import Foundation

class Network {
    static var instance = Network() // This is singleton
    var size = String()
    var comment = String()
    var imArr : [UIImage] = []
    var FIO = String()
    var PHONE = String()
    var EMAIL = String()
    var mailTo = String()
    var typeOf = String()
    
    
    typealias CompletionHandler = (_ success:Bool) -> Void
    var errorMess = String()
    
    func sendParams() {
        let url = URL(string: "http://ttpl.com.ua")
        mailTo = "info@habitare.ua"
//        mailTo = "goncharov.gleb@gmail.com"

        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=----WebKitFormBoundary825MUoTATMa7P9md", forHTTPHeaderField: "Content-Type")
        
        
        print("phone\(PHONE) email: \(EMAIL)")
        let body = NSMutableData()
        //define the data post parameter
        
        body.append("------WebKitFormBoundary825MUoTATMa7P9md\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"data\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("\r\n{\"ids\":\"qwpoty\",\"idu\":\"\(EMAIL)\",\"fio\":\"\(FIO)\",\"phone\":\"\(PHONE)\",\"size\":\"\(size)\",\"type\":\"\(typeOf)\",\"emailto\":\"\(mailTo)\",\"email\":\"\(EMAIL)\",\"send\":\"1\"}".data(using: String.Encoding.utf8)!)
        body.append("\r\n------WebKitFormBoundary825MUoTATMa7P9md--".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body as Data
        
        // return body as Data
        print("Fire....")
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            print("Complete")
            if error != nil
            {
                print("error upload : \(String(describing: error))")
//                responseHandler(nil)
                return
            }
            
            do
            {
                print("ANS: \(String(describing: NSString(data: data!, encoding: String.Encoding.utf8.rawValue)))")
                if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                {
                    print(json)
//                    responseHandler(json as NSDictionary?)
                }else
                {
                    print("Invalid Json")
                }
            }
            catch
            {
                print("Some Error")
            }
        }
        task.resume()
    }
    
    func sendImg(image: UIImage, count: Int) {
        let url = URL(string: "http://ttpl.com.ua")
        mailTo = "info@habitare.ua"
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"

        //define the data post parameter
        request.setValue("multipart/form-data; boundary=----WebKitFormBoundary825MUoTATMa7P9md", forHTTPHeaderField: "Content-Type")
        
        let body = NSMutableData()
        //define the data post parameter
        
        body.append("------WebKitFormBoundary825MUoTATMa7P9md\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"data\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("\r\n{\"ids\":\"qwpoty\",\"idu\":\"\(EMAIL)\",\"send\":\"0\"}".data(using: String.Encoding.utf8)!)
        body.append("\r\n------WebKitFormBoundary825MUoTATMa7P9md\r\n".data(using: String.Encoding.utf8)!)
        
//
        
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        
        let filename = "user-p\(count).jpg"
        let mimetype = "image/jpg"
        
//        body.appendString(string: "------WebKitFormBoundary825MUoTATMa7P9md\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"files\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageData!)
        body.appendString(string: "\r\n")
        body.appendString(string: "------WebKitFormBoundary825MUoTATMa7P9md--\r\n")
        request.httpBody = body as Data
        // return body as Data
        print("Fire....")
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            print("Complete")
            if error != nil
            {
                print("error upload : \(String(describing: error))")
                //                responseHandler(nil)
                return
            }
            
            do
            {
                print("ANS: \(String(describing: NSString(data: data!, encoding: String.Encoding.utf8.rawValue)))")
//                self.sendParams()
                if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                {
                    print(json)
                    //                    responseHandler(json as NSDictionary?)
                }else
                {
                    print("Invalid Json")
                }
            }
            catch
            {
                print("Some Error")
            }
        }
        task.resume()
    }
    class func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    /* request.HTTPMethod = "POST"
     
     var boundary = generateBoundaryString()
     request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
     
     var body = NSMutableData()
     
     if self.img.image != nil {
     var imageData = UIImagePNGRepresentation(self.img.image)
     
     if imageData != nil {
     body.appendString("--\(boundary)\r\n")
     body.appendString("Content-Disposition: form-data; name=\"image\"; filename=\"image.png\"\r\n")
     body.appendString("Content-Type: image/png\r\n\r\n")
     body.appendData(imageData!)
     body.appendString("\r\n")
     }
     
     }
     
     body.appendString("--\(boundary)--\r\n")
     request.setValue("\(body.length)", forHTTPHeaderField:"Content-Length")
     request.HTTPBody = body*/
    func imageUploadRequest(imageView: UIImageView, param: [String:String]?, completionHandler: @escaping CompletionHandler) {
        
        let myUrl = NSURL(string: "http://ttpl.com.ua");
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST"
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let imageData = UIImageJPEGRepresentation(imageView.image!, 1)
        
        //    if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "picture", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        let task =  URLSession.shared.dataTask(with: request as URLRequest,
                                               completionHandler: {
                                                (data, response, error) -> Void in
                                                if let data = data {
                                                    
                                                    // You can print out response object
                                                    print("******* response = \(String(describing: response))")
                                                    
                                                    print(data.count)
                                                    // you can use data here
                                                    
                                                    // Print out reponse body
                                                    let responseString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                                                    print("****** response data = \(responseString!)")
                                                    
                                                    let json =  try!JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
                                                    
                                                    print("json value \(String(describing: json))")
                                                    let er = json?.object(forKey: "error")
                                                    var flag = Bool()
                                                    
                                                    if er == nil
                                                    {
                                                        if let parseJSON = json {
                                                            // Now we can access value of First Name by its key
                                                            let access_token = parseJSON["access_token"] as? String
                                                            let ID_token = ((json?.object(forKey: "user")as! NSDictionary).object(forKey: "id")) as! String
                                                            
                                                            let defaults = UserDefaults.standard
                                                            defaults.set(access_token, forKey: "access_token")
                                                            defaults.set(ID_token, forKey: "ID_token")
                                                        }
                                                        //var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: &err)
                                                        
                                                        flag = true // true if download succeed,false otherwise
                                                    }
                                                    else{
                                                        flag = false
                                                    }
                                                    
                                                    completionHandler(flag)
                                                } else if let error = error {
                                                    print(error)
                                                    
                                                    let flag = false
                                                    completionHandler(flag)
                                                }
        })
        task.resume()
    }
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}
// extension for impage uploading

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
