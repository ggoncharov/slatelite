//
//  HeaderCollectionViewCell.swift
//  SlateLite
//
//  Created by Gotlib on 21.01.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit

class HeaderCollectionViewCell: UICollectionReusableView {
        
    @IBOutlet weak var labText: UILabel!
}
