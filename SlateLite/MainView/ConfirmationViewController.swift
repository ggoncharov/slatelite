//
//  ConfirmationViewController.swift
//  SlateLite
//
//  Created by Gotlib on 23.01.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit

class ConfirmationViewController: UIViewController {

    @IBOutlet weak var thanksTopTextLable: UILabel!
    @IBOutlet weak var thanksBottomTextLable: UILabel!

    @IBOutlet weak var goToMainButton: UIButton!
//    @IBOutlet weak var widthCon: NSLayoutConstraint!
    @IBOutlet weak var rightCon: NSLayoutConstraint!
    @IBOutlet weak var leftCon: NSLayoutConstraint!
    
    func printDevice() {
        if UIDevice().userInterfaceIdiom == .pad {
            print("Ipad")
//            self.view.frame = CGRect(x:0, y:0, width:500, height: self.view.frame.size.height)
            rightCon.constant = -100
            leftCon.constant = 100
        }
        else {
            print("Iphone")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.printDevice()
        goToMainButton.layer.cornerRadius = 10
        
        self.thanksTopTextLable.text = NSLocalizedString("Thanks_Top_Text", comment: "")
        self.thanksBottomTextLable.text = NSLocalizedString("Thanks_Bottom_Text", comment: "")
        self.goToMainButton.setTitle(NSLocalizedString("Go_to_main", comment: ""), for: .normal)
        let image = UIImage(named: "MainLogo")
        self.navigationItem.titleView = UIImageView(image: image)
        self.navigationController?.navigationBar.backgroundColor = .white
        self.view.backgroundColor = .white
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.tintColor = UIColor.UIColorFromRGB(rgbValue: 0x4C2F47)
        // Do any additional setup after loading the view.
    }

    @IBAction func goToMainAct(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        navigationController?.pushViewController(destination, animated: false)
    }
    
    @objc func infoAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
        navigationController?.pushViewController(destination, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
