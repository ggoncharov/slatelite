//
//  ColorsCollectionViewCell.swift
//  SlateLite
//
//  Created by Gotlib on 21.01.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit

class ColorsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var check: UIImageView!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var colorsMainImg: UIImageView!
    
    
    func setupCell(tile: Tile){
        
        self.colorsMainImg.layer.masksToBounds = true
        self.colorsMainImg.layer.cornerRadius = 10
        
        self.colorsMainImg.image = UIImage(named:tile.main)
        self.titleLab.text = tile.name
    }
    
    override var isSelected: Bool {
        didSet {
            self.colorsMainImg.layer.borderColor = UIColor.UIColorFromRGB(rgbValue: 0x353A40).cgColor
            self.colorsMainImg.layer.borderWidth = isSelected ? 2 : 0
            self.check.isHidden = !isSelected
        }
    }
}
