//
//  FavoritsTableViewCell.swift
//  SlateLite
//
//  Created by Gotlib on 02.05.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit

class FavoritsTableViewCell: UITableViewCell {

    @IBOutlet weak var favoritsImg: UIImageView!
    @IBOutlet weak var favoritsLab: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
