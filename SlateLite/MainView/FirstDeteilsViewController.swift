//
//  FirstDeteilsViewController.swift
//  SlateLite
//
//  Created by Gotlib on 23.01.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit
import SwiftPhotoGallery

class FirstDeteilsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, SwiftPhotoGalleryDataSource, SwiftPhotoGalleryDelegate
{
    
    /// total price lable
    @IBOutlet weak var totalPrice: UILabel!
    
    /// select size
    @IBOutlet weak var sizeButton: UIButton!
    @IBOutlet weak var arLab: UILabel!
    
    @IBOutlet weak var sizeSelectorPicker: UIPickerView!
    
    @IBOutlet weak var favoritsButton: UIButton!
    @IBOutlet weak var imheight: NSLayoutConstraint!
    @IBOutlet weak var widthCon: NSLayoutConstraint!
    @IBOutlet weak var rightCon: NSLayoutConstraint!
    @IBOutlet weak var leftCon: NSLayoutConstraint!

    @IBOutlet weak var youSelectTextLable: UILabel!
    /// scroll view
    @IBOutlet weak var scrollView: UIScrollView!
    /// main image of tile
    @IBOutlet weak var mainImg: UIImageView!
    /// tiles title lable
    @IBOutlet weak var titleLab: UILabel!
    /// tiles description
    @IBOutlet weak var descLab: UILabel!
    @IBOutlet weak var orderSizeLab: UILabel!
    
    @IBOutlet weak var squereTextLable: UILabel!
    /// squere text field
    @IBOutlet weak var squereTextField: UITextField!
    /// add comment text field
    @IBOutlet weak var commentsTextField: UITextField!
    /// go to next screen button
    @IBOutlet weak var sendButt: UIButton!
    /// collection view for images that user add
    @IBOutlet weak var collectionView: UICollectionView!
    
    /// open ARKit button
    @IBOutlet weak var goToARButton: UIButton!
    /// send button constraint from top
    @IBOutlet weak var sendFromTop: NSLayoutConstraint!
    
    
    @IBOutlet weak var sizeInLab: UILabel!
    @IBOutlet weak var sizeOutLab: UILabel!
    @IBOutlet weak var heightImg: NSLayoutConstraint!

    /// collection view of tile images
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    
    /// string with size to order
    var sizesToOrder = String()
    /// string with size avalible
    var sizesThatAvalible = String()
    /// array of avalible sizes for picker
    var sizeArray = [String]()

    var IsOpenArr: [UIImage] = []

    var imagePicker = UIImagePickerController()

    var infdj = Int()

    /// array of tile photos
    var imageArr = [UIImage]()
    
    /// current tile data
    var tile: Tile!
    
    /// check if favorits is taped
    var favoritsTap = Bool()
    /// array of saved favorits names
    var favoritsArray = [String]()
    /// price array
    var priceArray = [Int]()
    /// selected price
    var currentPrice = Int()
    
    
    func printDevice() {
        
        if UIDevice().userInterfaceIdiom == .pad {
            
             self.widthCon.constant = -200
             self.rightCon.constant = 100
             self.leftCon.constant = 100
            
//            let layout = imagesCollectionView!.collectionViewLayout as! UICollectionViewFlowLayout
//            layout.itemSize = CGSize(width: 94, height: 94)
//            self.imagesCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//            self.imagesCollectionView!.collectionViewLayout = layout
        }
        else{
            let layout = imagesCollectionView!.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: 94, height: 94)
            self.imagesCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.imagesCollectionView!.collectionViewLayout = layout

        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        Network.instance.size = self.squereTextField.text!
        Network.instance.comment = self.commentsTextField.text!
        Network.instance.imArr = self.IsOpenArr
        
        if let detailViewController = segue.destination as? DetailsViewController{
            detailViewController.sizesThatAvalible = self.sizesThatAvalible
            detailViewController.sizesToOrder = self.sizesToOrder
            detailViewController.tile = tile
            detailViewController.imageArr = self.imageArr
        }
    }
    
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        self.printDevice()
        self.hideKeyboardWhenTappedAround()
        
        self.sizeSelectorPicker.isHidden = true
        self.sizeSelectorPicker.backgroundColor = .white
        self.sizeSelectorPicker.delegate = self
        self.sizeSelectorPicker.dataSource = self
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.isUserInteractionEnabled = true
        
        self.imagesCollectionView.delegate = self
        self.imagesCollectionView.dataSource = self
        self.imagesCollectionView.isUserInteractionEnabled = true
        
        self.squereTextField.delegate = self
        self.squereTextField.keyboardType = UIKeyboardType.numberPad
        
        self.commentsTextField.delegate = self
        
        self.imagePicker.delegate = self
        
        self.sendButt.layer.cornerRadius = 10
        self.mainImg.layer.cornerRadius = 10
        self.mainImg.layer.masksToBounds = true
        
        self.goToARButton.layer.cornerRadius = 10
        self.arLab.layer.cornerRadius = 10
        self.arLab.layer.masksToBounds = true
        self.arLab.text = NSLocalizedString("Open_AR_Button_Text", comment: "")
        
        if #available(iOS 11.3, *) {
            
            ARViewController.sharedInstance.imageName = tile.main_1
        } else {
            
            // Fallback on earlier versions
        }
        
        
        
        guard let favor = UserDefaults.standard.object(forKey: "FavoritsList") else {return}
        favoritsArray = favor as! [String]
        if favoritsArray.contains(tile.name){
            
            self.favoritsButton.setImage(UIImage(named: "favorits_taped.png"), for: .normal)
            self.favoritsTap = false
        }
        else{
            
            self.favoritsTap = true
        }
        
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.UIColorFromRGB(rgbValue: 0x353A40)
        self.navigationController?.navigationBar.tintColor = UIColor.UIColorFromRGB(rgbValue: 0x4C2F47)
        
        self.IsOpenArr.append(UIImage(named:"addPhoto@3x")!)
        self.sendButt.alpha = 0.2
        self.collectionView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(false)

        let image = UIImage(named: "MainLogo")
        self.navigationItem.titleView = UIImageView(image: image)
        self.navigationController?.navigationBar.backgroundColor = .white
        self.view.backgroundColor = .white
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.UIColorFromRGB(rgbValue: 0x353A40)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.UIColorFromRGB(rgbValue: 0x353A40)
        
        self.descLab.text = NSLocalizedString("Available_sizes", comment: "")
        self.orderSizeLab.text = NSLocalizedString("Order_sizes", comment: "")
        self.youSelectTextLable.text = NSLocalizedString("You_Select", comment: "")
        self.sendButt.setTitle(NSLocalizedString("Next", comment: ""), for: .normal)
        self.squereTextLable.text = NSLocalizedString("Square_meters", comment: "")
        self.squereTextField.placeholder = NSLocalizedString("Square", comment: "")
        self.commentsTextField.placeholder = NSLocalizedString("Comment", comment: "")
        
        
        let layout = collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: 94, height: 94)
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.collectionView!.collectionViewLayout = layout
        
        self.sendButt.isEnabled = false
        
        self.titleLab.text = tile.name
        self.mainImg.image = UIImage(named: tile.main)
        
        let sizeIn = tile.sizeIn
        let price = tile.price
        switch MainPageDataSource.sharedInstance.current_open_list {
        case .slateLite:
            for i in price["SlateLite"]! as NSArray {
                self.priceArray.append(Int(i as! String)!)
            }
            
            for i in sizeIn["SlateLite"]! as NSArray {
                self.sizesThatAvalible = self.sizesThatAvalible + " \(i as! String)"
                self.sizeArray.append(i as! String)
            }
        case .ecostone:
            for i in price["ECOSTONE"]! as NSArray {
                self.priceArray.append(Int(i as! String)!)
            }
            
            for i in sizeIn["ECOSTONE"]! as NSArray {
                self.sizesThatAvalible = self.sizesThatAvalible + " \(i as! String)"
                self.sizeArray.append(i as! String)
            }
        case .trans:
            for i in price["TRANS"]! as NSArray {
                self.priceArray.append(Int(i as! String)!)
            }
            
            for i in sizeIn["TRANS"]! as NSArray {
                self.sizesThatAvalible = self.sizesThatAvalible + " \(i as! String)"
                self.sizeArray.append(i as! String)
            }
        }
        self.sizeInLab.text = sizesThatAvalible
        
        var sizeOut = tile.sizeOut
        switch MainPageDataSource.sharedInstance.current_open_list {
        case .slateLite:
            for i in sizeOut["SlateLite"]! as NSArray {
                self.sizesToOrder = self.sizesToOrder + " \(i as! String)"
            }
        case .ecostone:
            for i in sizeOut["ECOSTONE"]! as NSArray {
                self.sizesToOrder = self.sizesToOrder + " \(i as! String)"
            }
        case .trans:
            for i in sizeOut["TRANS"]! as NSArray {
                self.sizesToOrder = self.sizesToOrder + " \(i as! String)"
            }
        }
        self.sizeOutLab.text = sizesToOrder
        
        imageArr.append(UIImage(named: tile.main)!)
        for imageString in tile.photos
        {
            imageArr.append(UIImage(named: imageString)!)
        }

        if self.sizeArray.count != 0{
            
            self.sizeButton.setTitle(self.sizeArray[0], for: .normal)
            self.currentPrice = Int(self.priceArray[0])
        }
        else {
            
            self.sizeButton.setTitle("", for: .normal)
            self.currentPrice = 0
            self.sizeButton.isHidden = true
        }
    }
    
    // MARK: textField delegat methods
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == self.squereTextField {
            if self.squereTextField.text != ""
            {
                self.sendButt.isEnabled = true
                self.sendButt.alpha = 1
                let val = Int(self.squereTextField.text!)!
                let total = val * self.currentPrice
                if self.currentPrice != 0{
                    self.totalPrice.text = "~ \(total) евро"
                }
                else {
                    self.totalPrice.text = "Под заказ"
                }
            }
            else
            {
                self.sendButt.isEnabled = false
                self.sendButt.alpha = 0.2
            }
        }
        
        if textField == self.commentsTextField {
            self.scrollView.setContentOffset(CGPoint.zero, animated: true)
        }

    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.commentsTextField {
            let scrollPoint : CGPoint = CGPoint.init(x:0, y:textField.frame.origin.y - 100)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    

    // MARK: Actions
    @IBAction func selectSizeAction(_ sender: UIButton) {
        self.sizeSelectorPicker.isHidden = false
    }
    
    /// Add / remove from favorits
    @IBAction func favoritsTapAction(_ sender: UIButton) {
        
        if self.favoritsTap {
            if !self.favoritsArray.contains(tile.name){
                
                self.favoritsArray.append(self.tile.name)
                UserDefaults.standard.set(favoritsArray, forKey: "FavoritsList") //setObject
            }
            self.favoritsButton.setImage(UIImage(named: "favorits_taped.png"), for: .normal)
            self.favoritsTap = false
        }
        else{
            if let index = self.favoritsArray.index(of: self.tile.name){
                
                self.favoritsArray.remove(at: index)
            }
            UserDefaults.standard.set(favoritsArray, forKey: "FavoritsList") //setObject
            self.favoritsButton.setImage(UIImage(named: "favorits.png"), for: .normal)
            self.favoritsTap = true
        }
    }
    
    @objc func favoritsAction(){
    
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "FavoritsViewController") as! FavoritsViewController
        navigationController?.pushViewController(destination, animated: false)
    }
    
    @objc func infoAction() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
        navigationController?.pushViewController(destination, animated: false)
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func openAlertView(_ button: UIButton) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
       
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        self.present(alert, animated: true, completion: nil)

    }
    
    @IBAction func openGalary(_ sender: Any) {
        self.openGalery(selIngexGal:(sender as! UIButton).tag)
    }
    
    func openCamera() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.IsOpenArr.append(image)
        self.collectionView.reloadData()
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func openGalery(selIngexGal: Int){

        let gallery = SwiftPhotoGallery(delegate: self, dataSource: self)
        gallery.backgroundColor = UIColor.black
        gallery.pageIndicatorTintColor = UIColor.gray.withAlphaComponent(0.5)
        gallery.currentPageIndicatorTintColor = UIColor.white
        gallery.hidePageControl = false
        
         /// Or load on a specific page like this:
         present(gallery, animated: true, completion: { () -> Void in
         gallery.currentPage = selIngexGal
         })
 
    }
    
    // MARK: SwiftPhotoGalleryDataSource Methods
    func numberOfImagesInGallery(gallery: SwiftPhotoGallery) -> Int {
        
        return imageArr.count
    }
    
    func imageInGallery(gallery: SwiftPhotoGallery, forIndex: Int) -> UIImage? {
        
        return imageArr[forIndex]
    }
    
    // MARK: SwiftPhotoGalleryDelegate Methods
    func galleryDidTapToClose(gallery: SwiftPhotoGallery) {
        
        dismiss(animated: true, completion: nil)
    }
    
}


//extension FirstDeteilsViewController: UIGestureRecognizerDelegate {
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
//    }
//}


// MARK: - CollectionView
extension FirstDeteilsViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.imagesCollectionView {
            self.openGalery(selIngexGal:indexPath.item + 1)
        }
        else {
            let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.imagesCollectionView {
            return tile.photos.count
        }
        else {
            return IsOpenArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.imagesCollectionView {
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesCollectionViewCell", for: indexPath as IndexPath) as! imagesCollectionViewCell
            cell.imageView.image = UIImage(named: tile.photos[indexPath.row])
            cell.butt.addTarget(self, action: #selector(openGalary), for: .touchUpInside)
            cell.butt.tag = indexPath.item + 1
            return cell
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath as IndexPath) as! PhotoCollectionViewCell
            cell.imgM.image = IsOpenArr[indexPath.item]
            cell.butt.addTarget(self, action: #selector(openAlertView(_:)), for: .touchUpInside)
            return cell
        }
    }
}

extension FirstDeteilsViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.sizeArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return self.sizeArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        self.currentPrice = Int(self.priceArray[row])
        self.sizeButton.setTitle(self.sizeArray[row], for: .normal)
        self.sizeSelectorPicker.isHidden = true
        
        if self.squereTextField.text! != "" {
            let val = Int(self.squereTextField.text!)!
            let total = val * self.currentPrice
            if self.currentPrice != 0{
                self.totalPrice.text = "~ \(total) евро"
            }
            else {
                self.totalPrice.text = "Под заказ"
            }
        }
    }
}

class imagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var butt: UIButton!
    @IBOutlet weak var imageView: UIImageView!
}

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgM: UIImageView!
    @IBOutlet weak var butt: UIButton!
    
}
