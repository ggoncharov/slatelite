//
//  CallBackViewController.swift
//  SlateLite
//
//  Created by Gotlib on 13.05.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit

class CallBackViewController: UIViewController {

    @IBOutlet weak var topTextLable: UILabel!
    
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phonetextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.sendButton.layer.cornerRadius = 10
        
        // Do any additional setup after loading the view.
    }
    @IBAction func sendAction(_ sender: UIButton) {
        
        Network.instance.FIO = nameTextField.text!
        Network.instance.EMAIL = emailTextField.text!
        Network.instance.PHONE = phonetextField.text!
        Network.instance.typeOf = "CallBack"
        
        Network.instance.sendParams()
        
        let alert = UIAlertController(title: "Обратный званок заказан", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction.init(title: "Ok", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
