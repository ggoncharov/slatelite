//
//  FavoritsViewController.swift
//  SlateLite
//
//  Created by Gotlib on 02.05.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit

class FavoritsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var noDataText: UILabel!
    @IBOutlet weak var mTable: UITableView!
    /// array for names of favorit tiles
    var favoritsArr = [String]()
    /// current list of favorit tile
    var currentList = [Tile]()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Favorits", comment: "")
        let result = UserDefaults.standard.object(forKey: "FavoritsList")
        if result != nil {
            self.noDataText.isHidden = false
            self.mTable.isHidden = false
            self.favoritsArr = result as! [String]
        
        
            /// taking data of tiles
            let arr = MainPageDataSource.sharedInstance.currentData
            for tiledata in arr{
                for favoritsdata in self.favoritsArr{
                    if tiledata.name == favoritsdata{
                       self.currentList.append(tiledata)
                    }
                }
            }

            mTable.reloadData()
        }
        else {
            self.mTable.isHidden = true
            self.noDataText.isHidden = false
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
       
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return self.currentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = mTable.dequeueReusableCell(withIdentifier: "FavoritsTableViewCell", for: indexPath as IndexPath) as! FavoritsTableViewCell
        cell.favoritsLab.text = self.currentList[indexPath.row].name
        cell.favoritsImg.image = UIImage(named: self.currentList[indexPath.row].main)
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "FirstDeteilsViewController") as! FirstDeteilsViewController
        destination.tile = self.currentList[indexPath.row]
        navigationController?.pushViewController(destination, animated: false)
    }
}
