//
//  MainViewController.swift
//  SlateLite
//
//  Created by Gotlib on 21.01.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit
import SDWebImage



class MainViewController: UIViewController, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var topLeft: NSLayoutConstraint!
    @IBOutlet weak var topRight: NSLayoutConstraint!
    
    @IBOutlet weak var slateLiteButton: UIButton!
    @IBOutlet weak var ecostoneButton: UIButton!
    @IBOutlet weak var transButton: UIButton!
    
    @IBOutlet weak var hiddenView: UIView!
    @IBOutlet weak var hidenTitle: UILabel!
    @IBOutlet weak var hidenImg: UIImageView!
    @IBOutlet weak var orderButt: UIButton!
    @IBOutlet weak var collectionViewBig: UICollectionView!
    
   
//    var selectedArr = [Any]()
    
    var selected: Tile? {
        
        didSet {
            
            hiddenView.isHidden = self.selected == nil
            guard let current = self.selected else {return}
            self.hidenTitle.text = current.name
            self.hidenImg.image = UIImage(named: current.main)
        }
    }
    
    var isOpenF = false
    
    
    @IBAction func listSelectedButtonPress(_ sender: UIButton) {
        
        MainPageDataSource.sharedInstance.current_open_list = CurrentOpenList(rawValue: sender.tag)!
        self.setupButtonHighLight()

        self.collectionViewBig.reloadData()
    }
    
    func printDevice() {
        
        if UIDevice().userInterfaceIdiom == .pad {
            print("Ipad")
            self.view.frame = CGRect(x:0, y:0, width:500, height: self.view.frame.size.height)
            topLeft.constant = 100
            topRight.constant = 100
            let layout = self.collectionViewBig!.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: self.view.frame.size.width/3 - 20, height: self.view.frame.size.width/3 - 16 + 30)
            layout.minimumLineSpacing = 16
            self.collectionViewBig.contentInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
            
            self.collectionViewBig!.collectionViewLayout = layout
        }
        else {
            print("Iphone")
            let layout = self.collectionViewBig!.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: self.view.frame.size.width/2 - 20, height: self.view.frame.size.width/2 - 16 + 30)
            layout.minimumLineSpacing = 16
            self.collectionViewBig.contentInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
            
            self.collectionViewBig!.collectionViewLayout = layout
        }
 }
    
    //MARK: - Button setup
    
    internal func setupButton(_ button:UIButton){
        
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(red: 0.3, green: 0.18, blue: 0.28, alpha: 0.2).cgColor
    }
    
    internal func setupButtons(){
        
        self.setupButton(slateLiteButton)
        self.setupButton(ecostoneButton)
        self.setupButton(transButton)
    }
    
    internal func resetButtons(){
        
        self.slateLiteButton.backgroundColor = .white
        self.ecostoneButton.backgroundColor = .white
        self.transButton.backgroundColor = .white
    }
    
    internal func setupButtonHighLight(){
        
        self.resetButtons()
        var button: UIButton
        switch MainPageDataSource.sharedInstance.current_open_list {
            case .slateLite:
                button = self.slateLiteButton
            case .ecostone:
                button = self.ecostoneButton
            case .trans:
                button = self.transButton
        }
        
        if( button.layer.backgroundColor != UIColor(red: 0.3, green: 0.18, blue: 0.28, alpha: 0.1).cgColor){
            
            self.selected = nil
            self.isOpenF = false
        }
        
        button.layer.backgroundColor = UIColor(red: 0.3, green: 0.18, blue: 0.28, alpha: 0.1).cgColor
    }
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.printDevice()
        _ = MainPageDataSource.sharedInstance
        
        self.setupButtons()
        self.setupButtonHighLight()

        self.hidenImg.layer.cornerRadius = 5
        self.hidenImg.layer.masksToBounds = true
        self.orderButt.layer.cornerRadius = 10
        self.orderButt.setTitle(NSLocalizedString("Select_Button_Text", comment: ""), for: .normal)
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "MainLogo"))
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.UIColorFromRGB(rgbValue: 0x353A40)
        
//        let layout = self.collectionViewBig!.collectionViewLayout as! UICollectionViewFlowLayout
//        layout.itemSize = CGSize(width: self.view.frame.size.width/2 - 20, height: self.view.frame.size.width/2 - 16 + 30)
//        layout.minimumLineSpacing = 16
//        self.collectionViewBig.contentInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
//
//        self.collectionViewBig!.collectionViewLayout = layout

    }
    

    @objc func tapDetected() {
        
        self.isOpenF = !self.isOpenF
        self.collectionViewBig.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let firstDetailViewController = segue.destination as? FirstDeteilsViewController{
            
            guard let current = self.selected else {return}
            firstDetailViewController.tile = current
        }
    }

}


// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //self.data = MainPageDataSource.sharedInstance.currentData
        return MainPageDataSource.sharedInstance.currentData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorsCollectionViewCell", for: indexPath) as! ColorsCollectionViewCell
        
        cell.setupCell(tile: MainPageDataSource.sharedInstance.currentData[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selected = MainPageDataSource.sharedInstance.currentData[indexPath.row]
//        if self.selected == nil{
//            self.selected = MainPageDataSource.sharedInstance.currentData[indexPath.row]
//        }
//        else {
//
//            self.selected = nil
//            self.isOpenF = false
//            self.collectionViewBig.reloadData()
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout  collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        
        return CGSize(width:self.collectionViewBig.frame.width,height: self.isOpenF ? 170 : 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionViewCell", for: indexPath) as! HeaderCollectionViewCell
       sectionHeader.labText.text = HeaderStrings.headerText()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(tapDetected))
        sectionHeader.addGestureRecognizer(tapGestureRecognizer)
        
        return sectionHeader
    }
}

// MARK: - color from RGB
extension UIColor {
    class func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
