//
//  DetailsViewController.swift
//  SlateLite
//
//  Created by Gotlib on 23.01.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit
import SwiftPhotoGallery

class DetailsViewController: UIViewController, UITextFieldDelegate, SwiftPhotoGalleryDataSource, SwiftPhotoGalleryDelegate {
    
    @IBOutlet weak var imheight: NSLayoutConstraint!
    
    /// squere lable
    @IBOutlet weak var squerLabSize: UILabel!
    /// main scrollview
    @IBOutlet weak var scrollView: UIScrollView!
    /// main image
    @IBOutlet weak var mainImg: UIImageView!
    /// title lable
    @IBOutlet weak var titleLab: UILabel!
    /// description lable
    @IBOutlet weak var descLab: UILabel!
    /// users name textfiled
    @IBOutlet weak var nameTextField: UITextField!
    /// users email textfiled
    @IBOutlet weak var emailTextField: UITextField!
    /// users phone textfiled
    @IBOutlet weak var phoneTextField: UITextField!
    /// send button
    @IBOutlet weak var sendButt: UIButton!
    @IBOutlet weak var ordersSizeLable: UILabel!
    @IBOutlet weak var avalibleSizeTextLable: UILabel!
    
    
    
    @IBOutlet weak var youSelectTextLable: UILabel!
    @IBOutlet weak var infoTextLable: UILabel!
    
    @IBOutlet weak var quereLab: UILabel!
    @IBOutlet weak var sendFromTop: NSLayoutConstraint!
    @IBOutlet weak var widthCon: NSLayoutConstraint!
    @IBOutlet weak var rightCon: NSLayoutConstraint!
    @IBOutlet weak var leftCon: NSLayoutConstraint!
    
    /// size that avalible lable
    @IBOutlet weak var sizeInLab: UILabel!
    /// size to order lable
    @IBOutlet weak var sizeOutLab: UILabel!

    
    /// images collection view
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    
    /// tile data
    var tile: Tile!
    /// avalible sizes
    var sizesThatAvalible: String!
    /// order size
    var sizesToOrder: String!
    /// array of tile photos
    var imageArr = [UIImage]()
    
    /// detect device
    fileprivate func printDevice() {
        
        if UIDevice().userInterfaceIdiom == .pad {
            print("Ipad")
            self.widthCon.constant = -200
            self.rightCon.constant = 100
            self.leftCon.constant = 100
        }
        else {
            
            let layout = imagesCollectionView!.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: 94, height: 94)
            self.imagesCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.imagesCollectionView!.collectionViewLayout = layout
            }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.youSelectTextLable.text = NSLocalizedString("You_Select", comment: "")
        self.quereLab.text = NSLocalizedString("YouSetSquere", comment: "")
        self.quereLab.text = NSLocalizedString("Order_sizes", comment: "")
        self.infoTextLable.text = NSLocalizedString("EnterInfoText", comment: "")
        self.ordersSizeLable.text = NSLocalizedString("Order_sizes", comment: "")
        self.avalibleSizeTextLable.text = NSLocalizedString("Available_sizes", comment: "")
        self.nameTextField.placeholder = NSLocalizedString("Enter_Name", comment: "")
        self.emailTextField.placeholder = NSLocalizedString("Enter_Phone", comment: "")
        self.phoneTextField.placeholder = NSLocalizedString("Enter_Email", comment: "")
        self.sendButt.setTitle(NSLocalizedString("Send_request", comment: ""), for: .normal)
        
        
        let image = UIImage(named: "MainLogo")
        self.navigationItem.titleView = UIImageView(image: image)
        self.navigationController?.navigationBar.backgroundColor = .white
        self.view.backgroundColor = .white
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.UIColorFromRGB(rgbValue: 0x353A40)
        self.navigationController?.navigationBar.tintColor = UIColor.UIColorFromRGB(rgbValue: 0x4C2F47)
        
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.printDevice()

        self.imagesCollectionView.delegate = self
        self.imagesCollectionView.dataSource = self
        self.imagesCollectionView.isUserInteractionEnabled = true
        
        self.mainImg.image = UIImage(named: tile.main)
        self.titleLab.text = tile.name
        self.sizeInLab.text = self.sizesThatAvalible
        self.sizeOutLab.text = self.sizesToOrder
        self.squerLabSize.text = Network.instance.size + NSLocalizedString("Square_meters", comment: "")
        
        self.nameTextField.delegate = self
        self.phoneTextField.delegate = self
        self.emailTextField.delegate = self
        
        self.sendButt.alpha = 0.2
        self.sendButt.isEnabled = false

        self.mainImg.isUserInteractionEnabled = true
        self.sendButt.layer.cornerRadius = 10
        self.mainImg.layer.cornerRadius = 10
        self.mainImg.layer.masksToBounds = true

    }
    
    @objc func favoritsAction(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "FavoritsViewController") as! FavoritsViewController
        navigationController?.pushViewController(destination, animated: false)
    }
    
    @objc func infoAction() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
        navigationController?.pushViewController(destination, animated: false)
    }
    
    // MARK: textField delegat
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if self.nameTextField.text != "" && self.phoneTextField.text != ""
        {
            self.sendButt.isEnabled = true
            self.sendButt.alpha = 1
        }
        else
        {
            self.sendButt.isEnabled = false
            self.sendButt.alpha = 0.2
        }
        
        self.scrollView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
            let scrollPoint : CGPoint = CGPoint.init(x:0, y:textField.frame.origin.y - 100)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
    }
    
    // MARK: actions
    @objc func openGalery(selIngexGal: Int){

        let gallery = SwiftPhotoGallery(delegate: self, dataSource: self)

        gallery.backgroundColor = UIColor.black
        gallery.pageIndicatorTintColor = UIColor.gray.withAlphaComponent(0.5)
        gallery.currentPageIndicatorTintColor = UIColor.white
        gallery.hidePageControl = false

        present(gallery, animated: true, completion: { () -> Void in
            gallery.currentPage = selIngexGal
        })
    }
    @objc func openGalery1(_ sender:AnyObject){
        let gallery = SwiftPhotoGallery(delegate: self, dataSource: self)
        
        gallery.backgroundColor = UIColor.black
        gallery.pageIndicatorTintColor = UIColor.gray.withAlphaComponent(0.5)
        gallery.currentPageIndicatorTintColor = UIColor.white
        gallery.hidePageControl = false
        
        present(gallery, animated: true, completion: nil)
    }
    
    @IBAction func sendAct(_ sender: UIButton) {
        Network.instance.FIO = nameTextField.text!
        Network.instance.EMAIL = phoneTextField.text!
        Network.instance.PHONE = emailTextField.text!
        Network.instance.typeOf = titleLab.text!

        var count = 0
        for  img in Network.instance.imArr
        {
            count = count + 1
            print(img)
            Network.instance.sendImg(image: img, count: count)
        }
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            Network.instance.sendParams()
        self.performSegue(withIdentifier: "GoToConfirmation", sender: nil)

        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: SwiftPhotoGalleryDataSource Methods
    
    func numberOfImagesInGallery(gallery: SwiftPhotoGallery) -> Int {

        return imageArr.count
    }
    
    func imageInGallery(gallery: SwiftPhotoGallery, forIndex: Int) -> UIImage? {

        return imageArr[forIndex]
    }
    
    // MARK: SwiftPhotoGalleryDelegate Methods
    func galleryDidTapToClose(gallery: SwiftPhotoGallery) {
        
        dismiss(animated: true, completion: nil)
    }

    @IBAction func openGalary(_ sender: Any) {
        self.openGalery(selIngexGal:(sender as! UIButton).tag)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}

/// collection view cell
class imagesDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var butt: UIButton!
    @IBOutlet weak var imageView: UIImageView!
}

// MARK: - CollectionView
extension DetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return tile.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesDetailCollectionViewCell", for: indexPath as IndexPath) as! imagesDetailCollectionViewCell
        cell.imageView.image = UIImage(named: tile.photos[indexPath.row])
        cell.butt.addTarget(self, action: #selector(openGalary), for: .touchUpInside)
        cell.butt.tag = indexPath.item + 1
        return cell
    }
    
}

// MARK: extension for keyboard hide/show detection
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
