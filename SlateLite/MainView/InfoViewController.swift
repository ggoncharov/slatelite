//
//  InfoViewController.swift
//  SlateLite
//
//  Created by Gotlib on 22.01.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit
import MessageUI
import MapKit
import CoreLocation

class InfoViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var fTextLable: UILabel!
    
    @IBOutlet weak var sTextLable: UILabel!
    
    @IBOutlet weak var tTextLable: UILabel!
    
    @IBOutlet weak var howToConnectTextLable: UILabel!
    @IBOutlet weak var orderCallButton: UIButton!
    @IBOutlet weak var contactsTextLable: UILabel!
    

    @IBOutlet weak var scrollViewQ: UIScrollView!
    @IBOutlet weak var mapImg: UIImageView!
    @IBOutlet weak var mapKitView: MKMapView!
    
    @IBOutlet weak var rightPad: NSLayoutConstraint!
    @IBOutlet weak var leftPad: NSLayoutConstraint!
    @IBOutlet weak var widthPad: NSLayoutConstraint!
    
    @IBOutlet weak var secondphoneButton: UIButton!
    @IBOutlet weak var firstphoneButton: UIButton!
    
    @IBAction func sendMail(_ sender: UIButton) {
        
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setToRecipients([])
        mailVC.setSubject("")
        mailVC.setToRecipients(["info@habitare.ua"])
        mailVC.setMessageBody("", isHTML: false)
        
        present(mailVC, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func firstPhone(_ sender: UIButton) {
        
        guard let number = URL(string: "tel://+380487373483") else { return }
        UIApplication.shared.open(number)
    }
    
    @IBAction func secondPhone(_ sender: UIButton) {
        
        guard let number = URL(string: "tel://+380688254435") else { return }
        UIApplication.shared.open(number)
    }
    
    let attributes : [NSAttributedStringKey: Any] = [
        NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15),
        NSAttributedStringKey.foregroundColor : UIColor.blue,
        NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]


    
    var attributedString = NSMutableAttributedString(string:"")
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.howToConnectTextLable.text = NSLocalizedString("How_To_Connect", comment: "")
        self.contactsTextLable.text = NSLocalizedString("Contacts", comment: "")
        self.orderCallButton.setTitle(NSLocalizedString("Order_call", comment: ""), for: .normal)
        let buttonTitleStr = NSMutableAttributedString(string:(self.firstphoneButton.titleLabel?.text)!, attributes:self.attributes)
        self.attributedString.append(buttonTitleStr)
        
        self.fTextLable.text = NSLocalizedString("F_text", comment: "")
        self.sTextLable.text = NSLocalizedString("S_text", comment: "")
        self.tTextLable.text = NSLocalizedString("T_text", comment: "")
        self.firstphoneButton.setAttributedTitle(self.attributedString, for: .normal)

        let buttonTitleStr2 = NSMutableAttributedString(string:(self.secondphoneButton.titleLabel?.text)!, attributes:self.attributes)
        self.attributedString.append(buttonTitleStr2)
        self.secondphoneButton.setAttributedTitle(self.attributedString, for: .normal)
        
        if UIDevice().userInterfaceIdiom == .pad {
            self.leftPad.constant = 100
        }
        let location = CLLocationCoordinate2DMake(46.427760, 30.729558)
        let span = MKCoordinateSpanMake(0.02, 0.02)
        
        let region = MKCoordinateRegionMake(location, span)
        
        self.mapKitView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        //annotation.setCoordinate(location)
        annotation.coordinate = location
        annotation.title = "Slate Lite"
        annotation.subtitle = ""
        
        self.mapKitView.addAnnotation(annotation)
        
        //Showing the device location on the map
        self.mapKitView.showsUserLocation = false
        
        // Do any additional setup after loading the view.
        let image = UIImage(named: "MainLogo")
        self.navigationItem.titleView = UIImageView(image: image)
        self.navigationController?.navigationBar.backgroundColor = .white
        self.view.backgroundColor = .white
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.tintColor = UIColor.UIColorFromRGB(rgbValue: 0x4C2F47)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
